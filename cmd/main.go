package main

import (
	"bufio"
	"emperror.dev/emperror"
	"emperror.dev/errors"
	"flag"
	"fmt"
	ironmaiden "github.com/je4/indexer/v2/pkg/indexer"
	"github.com/je4/utils/v2/pkg/checksum"
	iou "github.com/je4/utils/v2/pkg/io"
	datasiegfried "gitlab.switch.ch/ub-unibas/indexertest/v2/data/siegfried"
	"io"
	"os"
	"strings"
	"sync"
)

var input = flag.String("input", "", "input file")
var output = flag.String("output", "", "output file")

func main() {
	flag.Parse()
	println(*input)
	println(*output)

	var writer = []*iou.WriteIgnoreCloser{}

	relevance := map[int]ironmaiden.MimeWeightString{}

	// initialize action dispatcher
	ad := ironmaiden.NewActionDispatcher(relevance)
	_ = ironmaiden.NewActionSiegfried("siegfried", datasiegfried.DefaultSig, map[string]string{}, nil, ad)

	// todo: initialize other actions

	inFP, err := os.Open(*input)
	if err != nil {
		emperror.Panic(errors.Errorf("cannot open input file %s", *input))
	}
	defer inFP.Close()

	outFP, err := os.Create(*output)
	if err != nil {
		emperror.Panic(errors.Errorf("cannot open output file %s", *output))
	}
	writer = append(writer, iou.NewWriteIgnoreCloser(outFP))
	// defer outFP.Close()

	wg := sync.WaitGroup{}
	mimeReader, err := iou.NewMimeReader(inFP)
	if err != nil {
		emperror.Panic(errors.Wrap(err, "cannot create mime reader"))
	}
	contentType, _ := mimeReader.DetectContentType()
	parts := strings.Split(contentType, ";")
	contentType = parts[0]

	results := make(chan *ironmaiden.ResultV2, 1)
	indexerReader, indexerWriter := io.Pipe()
	writer = append(writer, iou.NewWriteIgnoreCloser(indexerWriter))
	wg.Add(1)
	go func() {
		defer wg.Done()
		result, err := ad.Stream(indexerReader, *input, []string{"siegfried"})
		if err != nil {
			results <- &ironmaiden.ResultV2{Errors: map[string]string{"unknown": err.Error()}}
			return
		}
		results <- result
	}()

	var ws = []io.Writer{}
	for _, w := range writer {
		ws = append(ws, bufio.NewWriterSize(w, 1024*1024))
		//		ws = append(ws, w)
	}

	csWriter, err := checksum.NewChecksumWriter([]checksum.DigestAlgorithm{checksum.DigestBlake2b384, checksum.DigestSHA512}, ws...)
	if err != nil {
		emperror.Panic(err)
	}

	written, err := io.Copy(csWriter, mimeReader)
	csWriter.Close()
	if err != nil {
		emperror.Panic(err)
	}
	println(written)
	for _, w := range ws {
		// it's sure, that w is a bufio.Writer
		if err1 := w.(*bufio.Writer).Flush(); err1 != nil {
			emperror.Panic(errors.Errorf("cannot flush writer: %v", err1))
		}
	}
	for _, w := range writer {
		w.ForceClose()
	}
	// error of copy
	if err != nil {
		emperror.Panic(errors.Errorf("cannot copy input to output: %v", err))
	}
	// wait for all actions to finish
	wg.Wait()
	close(results)
	result := ironmaiden.NewResultV2()
	for r := range results {
		result.Merge(r)
	}
	checksums, err := csWriter.GetChecksums()
	if err != nil {
		emperror.Panic(err)
	}

	fmt.Printf("Result: %v", result)
	fmt.Printf("Checksums: %v", checksums)
}
